import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SelectiveRepeat {
    private static DatagramPacket[] packets;

    public static void main(String[] args) throws Exception {

        Scanner s = new Scanner(System.in);

        Random random = new Random();
        ConcurrentLinkedQueue<DatagramPacket> packets = new ConcurrentLinkedQueue();
        DatagramSocket clientSocket = new DatagramSocket(9877);

        System.out.println("Indique o tamanho da janela");
        int windowSize = s.nextInt();
        System.out.println("Indique a probabilidade de perder um pacote");
        int prob = s.nextInt();

        String servidor = "localhost";
        int porta = 9876;

        InetAddress IPAddress = InetAddress.getByName(servidor);

        byte[] receiveData = new byte[1024];

        int received = 0;
        int confirmed = 0;
        DatagramPacket receivePacket = new DatagramPacket(receiveData,
                receiveData.length);
        System.out.println("Esperando por pacotes:");
        while (true) {
            clientSocket.receive(receivePacket);
            System.out.println("Recebeu: " + new String(receivePacket.getData()));
            Packet packet = (Packet) Serializer.toObject(receivePacket.getData());
            boolean isWindowed = false;
            Iterator iterator = packets.iterator();

            while (iterator.hasNext()){
                DatagramPacket listPacket = (DatagramPacket) iterator.next();
                Packet packetData = (Packet) Serializer.toObject(listPacket.getData());
                isWindowed = packet.getId() == packetData.getId();
            }



            if (packets.size() < windowSize && !isWindowed) {
                packets.add(receivePacket);
            }
            received++;

            System.out.println("Recebidos: " + received);

            if (random.nextInt(100) > prob) {
                Iterator packetIterator = packets.iterator();
                while (packetIterator.hasNext()){
                    DatagramPacket iteratorPacket = (DatagramPacket) packetIterator.next();
                    clientSocket.send(new DatagramPacket(iteratorPacket.getData(), iteratorPacket.getData().length, IPAddress, porta));
                    confirmed++;
                    packets.remove(iteratorPacket);

                }
            }

            System.out.println("Confirmados: " + confirmed);
            clearBuffer(receivePacket.getData());
        }
    }

    private static void clearBuffer(byte[] bytes) {
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = 0;
        }
    }
}
