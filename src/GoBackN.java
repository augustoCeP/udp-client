import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.Scanner;


public class GoBackN {
    public static void main(String[] args) throws Exception {

        Scanner s = new Scanner(System.in);

        Random random = new Random();

        DatagramSocket clientSocket = new DatagramSocket(9877);

        System.out.println("Indique a probabilidade de perder um pacote");
        int prob = s.nextInt();
        String servidor = "localhost";
        int porta = 9876;

        InetAddress IPAddress = InetAddress.getByName(servidor);

        byte[] receiveData = new byte[1024];

        int received = 0;
        int confirmed = 0;
        DatagramPacket receivePacket = new DatagramPacket(receiveData,
                receiveData.length);
        System.out.println("Esperando por pacotes:");
        while (true) {
            clientSocket.receive(receivePacket);
            received++;
            System.out.println("Recebeu: " + new String(receivePacket.getData()));
            System.out.println("Recebidos: " + received);
            if (random.nextInt(100) > prob) {
                clientSocket.send(new DatagramPacket(receivePacket.getData(), receivePacket.getData().length, IPAddress, porta));
                confirmed++;
            }
            System.out.println("Confirmados: " + confirmed);
           clearBuffer(receivePacket.getData());
        }
    }

    private static void clearBuffer(byte[] bytes){
        for (int i = 0; i < bytes.length; i++){
            bytes[i] = 0;
        }
    }
}
